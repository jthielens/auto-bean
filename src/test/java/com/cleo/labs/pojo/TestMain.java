package com.cleo.labs.pojo;

import static org.junit.Assert.*;

import java.util.List;
import java.util.NavigableSet;

import com.cleo.labs.pojo.CuPrinter;
import com.cleo.labs.pojo.GoogleSheets;
import com.cleo.labs.pojo.Tokens;

import org.junit.Test;

public class TestMain {
    private static final String LOGIN = "cleo.build@gmail.com";

    @Test
    public void test() {
        assertTrue(CuPrinter.validIdentifier("True"));
        assertFalse(CuPrinter.validIdentifier("true"));
    }

    @Test
    public void testGet() {
        String text = Tokens.get(LOGIN).authURL().url(UrlBuilder.Method.GET);
        System.out.println(text);
        assertNotNull(text);
    }

    @Test
    public void testInit() {
        String login = null; // LOGIN;
        Tokens i = GoogleSheets.interactiveInit(login);
        Tokens t = Tokens.get(i.login());
        assertNotNull(t.refresh_token());
    }

    @Test
    public void testGetSheets() {
        List<Sheet> sheets = Sheet.getSheets(LOGIN);
        sheets.forEach(System.out::println);
        assertEquals(1, sheets.size());
        List<Tab> tabs = sheets.get(0).getTabs();
        tabs.forEach(System.out::println);
        assertEquals(5, tabs.size());
        NavigableSet<Cell> cells = tabs.get(0).cells();
        assertNotNull(cells);
        cells.forEach(System.out::println);
    }
}
