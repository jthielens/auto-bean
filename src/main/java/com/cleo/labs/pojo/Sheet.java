package com.cleo.labs.pojo;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

@Setter @Getter @ToString @Accessors(fluent = true)
public class Sheet {
    private String login = null;
    private String id    = null;
    private String title = null;
    private String feed  = null;

    public Sheet(String login, Node node) {
        try {
            login(login);
            id   (XPATH.evaluate("id",                          node));
            title(XPATH.evaluate("title",                       node));
            feed (XPATH.evaluate("link[@rel='"+FEED+"']/@href", node));
        } catch (XPathExpressionException e) {
            e.printStackTrace(System.err);
        }
    }

    protected static final XPath           XPATH   = XPathFactory.newInstance().newXPath();
    protected static final DocumentBuilder BUILDER = newBuilder();
    private static DocumentBuilder newBuilder() {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (Exception ignore) {}
        return null;
    }

    private static final String FEED  = "http://schemas.google.com/spreadsheets/2006#worksheetsfeed";

    public static List<Sheet> getSheets(String login) {
        List<Sheet> sheets = new ArrayList<>();
        try {
            String response = new UrlBuilder("https://spreadsheets.google.com/feeds/spreadsheets/private/full")
                                  .auth(Tokens.get(login).googleAuth)
                                  .get();
            Document xml = BUILDER.parse(new InputSource(new StringReader(response)));
            NodeList entries = (NodeList) XPATH.evaluate("/feed/entry", xml, XPathConstants.NODESET);
            for (int i=0; i<entries.getLength(); i++) {
                sheets.add(new Sheet(login, entries.item(i)));
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return sheets;
    }

    public static Sheet getSheet(String login, String title) {
        return getSheets(login).stream().filter((s)->s.title().equalsIgnoreCase(title)).findFirst().orElse(null);
    }

    public List<Tab> getTabs() {
        List<Tab> tabs = new ArrayList<>();
        try {
            String response = new UrlBuilder(feed())
                                  .auth(Tokens.get(login).googleAuth)
                                  .get();
            Document xml = BUILDER.parse(new InputSource(new StringReader(response)));
            NodeList entries = (NodeList) XPATH.evaluate("/feed/entry", xml, XPathConstants.NODESET);
            for (int i=0; i<entries.getLength(); i++) {
                tabs.add(new Tab(login(), entries.item(i)));
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return tabs;
    }

    /**
     * Returns the first tab whose title matches {@code title} (case
     * insensitively), or {@code null} if no such tab is found.
     * @param title the title to find
     * @return the first matching tab, or {@code null}
     */
    public Tab getTab(String title) {
        return getTabs().stream().filter((t)->t.title().equalsIgnoreCase(title)).findFirst().orElse(null);
    }

    /**
     * Pretty prints an XML {@link Node} into a String.
     * @param doc the {@link Node} to format
     * @return the formatted String
     */
    protected static String x2s(Node doc) {
        try{
             DOMSource domSource = new DOMSource(doc);
             StringWriter writer = new StringWriter();
             StreamResult result = new StreamResult(writer);
             TransformerFactory tf = TransformerFactory.newInstance();
             Transformer transformer = tf.newTransformer();
             transformer.setOutputProperty(OutputKeys.INDENT, "yes");
             transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
             transformer.transform(domSource, result);
             writer.flush();
             return writer.toString();
        } catch (Exception e) { return null; }
    }

}
