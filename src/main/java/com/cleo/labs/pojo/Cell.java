package com.cleo.labs.pojo;

import javax.xml.xpath.XPathExpressionException;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import org.w3c.dom.Node;

@Setter @Getter @ToString @Accessors(fluent = true)
public class Cell implements Comparable<Cell> {
    private String title  = null;
    private int    row    = 0;
    private int    col    = 0;
    private String input  = null;
    private String number = null;
    private String value  = null;

    public Cell(Node node) {
        try {
            title(Sheet.XPATH.evaluate("title", node));
            row(Integer.valueOf(Sheet.XPATH.evaluate("cell/@row", node)));
            col(Integer.valueOf(Sheet.XPATH.evaluate("cell/@col", node)));
            input(Sheet.XPATH.evaluate("cell/@inputValue", node));
            number(Sheet.XPATH.evaluate("cell/@numericValue", node));
            value(Sheet.XPATH.evaluate("cell", node));
        } catch (XPathExpressionException e) {
            e.printStackTrace(System.err);
        }
    }
    @Override
    public int compareTo(Cell o) {
        return row()==o.row() ? col()-o.col() : row()-o.row();
    }
}
