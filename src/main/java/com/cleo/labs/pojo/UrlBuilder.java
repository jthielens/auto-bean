package com.cleo.labs.pojo;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

/**
 * Little helper class to make uri?option=value&... easier
 * to read, deal with GET/POST differences, and an abstraction
 * to deal with OAuth 2.0 refresh tokens.
 */
public class UrlBuilder {
    /**
     * A refreshable bearer token.
     */
    public interface Auth {
        /**
         * Returns the current bearer token to use.
         * @return a bearer token
         */
        public String token();
        /**
         * Update the current bearer token.  This method
         * is called if the server returns a 401 using the
         * previous {@link #token()}.
         */
        public void   refresh();
    }
    private String        endpoint = null;
    private StringBuilder query    = new StringBuilder();
    private Auth          auth     = null;
    /**
     * Construct a new request rooted at an endpoint,
     * e.g. {@code http://host:port/path}.
     * @param endpoint the root URL
     */
    public UrlBuilder(String endpoint) {
        this.endpoint = endpoint;
    }
    /**
     * Add a parameter to this request.  For GET, this will
     * be encoded on the request line {@code ?option=value&...},
     * but for PUT this will appear in the POST body.
     * @param option the option, not {@code null}
     * @param value the value, possibly {@code null}
     * @return {@code this}
     */
    public UrlBuilder opt(String option, String value) {
        // TODO: url encoding?
        if (query.length()>0) query.append('&');
        query.append(option);
        if (value!=null) {
            query.append('=').append(value);
        }
        return this;
    }
    /**
     * Add an {@link Auth} token provider to this request.
     * @param auth the {@link Auth}
     * @return {@code this}
     */
    public UrlBuilder auth(Auth auth) {
        this.auth = auth;
        return this;
    }
    protected String url(Method method) {
        if (method==Method.POST || query.length()==0) {
            return endpoint;
        } else {
            return new StringBuilder(endpoint).append('?').append(query).toString();
        }
    }
    /**
     * Simple selection between HTTP methods {@link Method#GET} and
     * {@link Method#POST}.
     */
    public enum Method {
        /**
         * Use HTTP GET.
         */
        GET,
        /**
         * Use HTTP POST.
         */
        POST};
    /**
     * Describes the HTTP debug level, which controls the kind of
     * verbose trace messages printed to {@link System#err}, with the
     * choices being "always" or "only in case of error", with two
     * flavors of "error" (anything but 200, or anything but 200 or 401).
     */
    public enum Debug  {
        /**
         * Print all debug messages.
         */
        ALWAYS,
        /**
         * Suppress debug messages if the result is 200 OK or 401 UNAUTHORIZED.
         */
        NOTOK401,
        /**
         * Suppress debug messages if the result is 200 OK.
         */
        NOTOK};
    /**
     * Shorthand for {@link #request(Method, Debug)} using
     * {@link Method#GET} and {@link Debug#NOTOK401}.
     * @return the response text, possibly {@code null} in case of error.
     */
    public String get() {
        return request(Method.GET, Debug.NOTOK401);
    }
    /**
     * Shorthand for {@link #request(Method, Debug)} using
     * {@link Method#GET} and {@link Debug} as indicated.
     * @param debug the {@link Debug} level to use
     * @return the response text, possibly {@code null} in case of error.
     */
    public String get(Debug debug) {
        return request(Method.GET, debug);
    }
    /**
     * Shorthand for {@link #request(Method, Debug)} using
     * {@link Method#POST} and {@link Debug#NOTOK401}.
     * @return the response text, possibly {@code null} in case of error.
     */
    public String post() {
        return request(Method.POST, Debug.NOTOK401);
    }
    /**
     * Shorthand for {@link #request(Method, Debug)} using
     * {@link Method#POST} and {@link Debug} as indicated.
     * @param debug the {@link Debug} level to use
     * @return the response text, possibly {@code null} in case of error.
     */
    public String post(Debug debug) {
        return request(Method.POST, debug);
    }
    /**
     * Processes the requested HTTP {@link Method} at the
     * requested {@link Debug} level, returning the response
     * body as a {@Link String} (possibly {@code null} in case
     * of error).  Error messages as requested by {@code debug}
     * are printed on {@link System#err}.
     * @param method the requested {@link Method}
     * @param debug the requested {@link Debug}
     * @return the response text, possibly {@code null} in case of error.
     */
    public String request(Method method, Debug debug) {
        String  text    = null;
        boolean retried = false;
        try {
            for(;;) {
                HttpURLConnection http = (HttpURLConnection) new URL(url(method)).openConnection();
                if (method==Method.POST) {
                    http.setRequestMethod("POST");
                }
                http.setRequestProperty("User-Agent", "curl/7.37.1");
                http.setUseCaches(false);
                if (auth!=null) {
                    http.setRequestProperty("Authorization", "Bearer "+auth.token());
                }
                if (method==Method.POST) {
                    http.setDoOutput(true);
                    OutputStream body = http.getOutputStream();
                    body.write(query.toString().getBytes(Charsets.UTF_8));
                    body.close();
                }
                try {
                    InputStream input = http.getInputStream();
                    if (input!=null) {
                        try (InputStreamReader reader = new InputStreamReader(input)) {
                            text = CharStreams.toString(reader);
                        }
                    }
                } catch (IOException ioe) {
                    // must have had trouble
                }
                debugHttpResponse(http, text, debug);
                if (auth!=null && !retried && http.getResponseCode()==HttpURLConnection.HTTP_UNAUTHORIZED) {
                    auth.refresh();
                    retried = true;
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return text;
    }
    /**
     * Prints one or more messages on stderr.
     * @param ss the (possibly {@code null}) list of messages to print
     */
    private static void debug(String...ss) {
        if (ss!=null) {
            Arrays.asList(ss).forEach(System.err::println);
        }
    }

    /**
     * Dumps everything about an HTTP response on stderr.
     * @param http the {@code HttpUrlConnection} to debug
     * @param text the already-slurped response body
     * @param always {@code false} means debug only when not 200 or 401
     */
    protected static void debugHttpResponse(HttpURLConnection http, String text, Debug debug) {
        try {
            if (debug==Debug.ALWAYS ||
                debug==Debug.NOTOK &&
                    http.getResponseCode()!=HttpURLConnection.HTTP_OK ||
                debug==Debug.NOTOK401 &&
                    http.getResponseCode()!=HttpURLConnection.HTTP_OK &&
                    http.getResponseCode()!=HttpURLConnection.HTTP_UNAUTHORIZED) {
                debug(http.getResponseCode()+": "+http.getResponseMessage());
                http.getHeaderFields().entrySet().forEach((header) ->
                   header.getValue().forEach((value) ->
                      debug(header.getKey()+": "+value)));
                InputStream errors = http.getErrorStream();
                if (errors!=null) {
                    debug("error:");
                    try (InputStreamReader reader = new InputStreamReader(errors)) {
                        debug(CharStreams.toString(reader));
                    }
                }
                if (text!=null) {
                    debug("output:", text);
                }
            }
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

}