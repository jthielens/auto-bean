package com.cleo.labs.pojo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.stream.Collectors;

import com.cleo.labs.util.repl.REPL;
import com.cleo.labs.util.repl.annotation.Command;

public class GoogleSheets extends REPL {

    private String           login = null;
    private Sheet            sheet = null;
    private Tab              tab   = null;

    @Command(name="login", args="id", comment="login using Google id")
    public void login(String id) {
        Tokens tokens = interactiveInit(id);
        login = tokens.login();
        if (login==null) {
            error("login as "+id+" not successful");
        } else {
            report(id+" logged in");
        }
    }

    @Command(name="sheets", comment="list sheets")
    public void sheets() {
        if (login==null) {
            error("not logged in");
        } else {
            Sheet.getSheets(login).forEach((s)->report(s.title()));
        }
    }

    @Command(name="tabs", args="[sheet-title]", comment="list tabs of selected sheet")
    public void tabs(String...titles) {
        if (select("tabs", 1, titles)) {
            sheet.getTabs().forEach((t)->report(t.title()));
        }
    }

    @Command(name="select", args="sheet-title [tab-title]", comment="select sheet or tab")
    public void select(String...titles) {
        if (titles==null || titles.length==0) {
            error("usage: select sheet-title [tab-title]");
        } else if (titles.length==1) {
            if (select("select", 1, titles)) {
                report("sheet "+sheet.title()+" selected");
            }
        } else {
            if (select("select", 2, titles)) {
                report("tab "+tab.title()+" on sheet "+sheet.title()+" selected");
            }
        }
    }

    public boolean select(String command, int need, String...titles) {
        if (titles!=null && titles.length>need) {
            StringBuilder e = new StringBuilder("usage: ").append(command);
            if (need>=1) {
                e.append(" [sheet-title");
                if (need>=2) {
                    e.append(" [tab-title]");
                }
                e.append(']');
            }
            error(e.toString());
        } else if (login==null) {
            error("not logged in");
        } else {
            if (need<1) {
                return true;
            }
            if (titles==null || titles.length<1 || titles[0].isEmpty()) {
                if (sheet==null && need>=1) {
                    error("no sheet selected");
                }
            } else {
                sheet = Sheet.getSheet(login, titles[0]);
                if (sheet==null) {
                    error("sheet "+titles[0]+" not found");
                }
            }
            if (sheet!=null) {
                if (need<2) {
                    return true;
                }
                if (titles==null || titles.length<2 || titles[1].isEmpty()) {
                    if (tab==null && need>=2) {
                        error("no tab selected");
                    }
                } else {
                    tab = sheet.getTab(titles[1]);
                    if (tab==null) {
                        error("tab "+titles[1]+" not found");
                    }
                }
                if (tab!=null) {
                    return true;
                }
            }
        }
        return false;
    }

    @Command(name="cells", args="[sheet-title [tab-title]]", comment="list cells")
    public void cells(String...titles) {
        if (select("cells", 2, titles)) {
            tab.cells().forEach((c)->report(c.toString()));
        }
    }

    @Command(name="size", args="[sheet-title [tab-title]]", comment="stats on a tab")
    public void size(String...titles) {
        if (select("size", 2, titles)) {
            int max_row = tab.cells().stream().collect(Collectors.summarizingInt(Cell::row)).getMax();
            int max_col = tab.cells().stream().collect(Collectors.summarizingInt(Cell::col)).getMax();
            report("rows="+max_row+" cols="+max_col+" count="+tab.cells().size()+" density="+(int)(100*tab.cells().size()/(max_row*max_col))+"%");
        }
    }

    @Command(name="row", args="i", comment="display row i")
    public void row(String i) {
        if (tab==null) {
            error("no tab loaded");
        } else {
            tab.row(Integer.valueOf(i)).forEach((c)->report(c.toString()));
        }
    }

    @Command(name="col", args="i", comment="display column i")
    public void col(String i) {
        if (tab==null) {
            error("no tab loaded");
        } else {
            tab.col(Integer.valueOf(i)).forEach((c)->report(c.toString()));
        }
    }

    public static Tokens interactiveInit(String login) {
        return interactiveInit(null, null, login, false);
    }

    public static Tokens interactiveInit(InputStream in, PrintStream out, String login, boolean force) {
        if (in==null) in = System.in;
        if (out==null) out = System.out;
        Tokens tokens = Tokens.get(login);
        BufferedReader input = new BufferedReader(new InputStreamReader (in));
        try {
            if (tokens.login()==null) {
                out.println("Enter Google login id:");
                tokens.login(input.readLine());
            }
            if (tokens.client_id()==null || force) {
                out.println("From https://console.developers.google.com, create a project\n"+
                            "and Add Credentials for an OAuth 2.0 client ID of type Other.");
                out.println("Enter client id:");
                tokens.client_id(input.readLine());
            }
            if (tokens.client_secret()==null || force) {
                out.println("Enter client secret:");
                tokens.client_secret(input.readLine());
            }
            if (tokens.refresh_token()==null || force) {
                if (tokens.login()==null || tokens.client_id()==null || tokens.client_secret()==null) {
                    out.println("login, client_id and client_secret are required");
                } else {
                    out.println("Use the following URL to obtain an access code:");
                    out.println(tokens.authURL().url(UrlBuilder.Method.GET));
                    out.println("Enter access code:");
                    String auth_code = input.readLine();
                    String access_token = tokens.accessFromAuthURL(auth_code).post(UrlBuilder.Debug.NOTOK);
                    tokens.set(access_token);
                }
            }
        } catch (Exception e) {
            e.printStackTrace(out);
        }
        return tokens;
    }

    public static void main(String[] argv) {
        GoogleSheets repl = new GoogleSheets();
        repl.run(argv);
        System.exit(0);
    }
}