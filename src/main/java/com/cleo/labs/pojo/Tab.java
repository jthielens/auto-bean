package com.cleo.labs.pojo;

import java.io.StringReader;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Representation of a Tab on a Google Sheet, identified by the
 * login account, the sheet title, and the tab title.  The (sparse
 * grid of) cells can be read as rows, columns, or an entire sheet.
 * This class supports a read-only view of the Tab.
 */
@ToString(exclude={"cells","rows","cols"}) @Accessors(fluent = true)
public class Tab {
    @Setter @Getter private String login = null;
    @Setter @Getter private String id    = null;
    @Setter @Getter private String title = null;
    @Setter @Getter private String feed  = null;

    // these are cached/derived
    private TreeSet<Cell> cells = null;
    private int           rows  = 0;
    private int           cols  = 0;

    /**
     * The Google Sheets link rel type for the cells feed.
     */
    private static final String CELLS = "http://schemas.google.com/spreadsheets/2006#cellsfeed";

    /**
     * Creates a new Tab from a {@code feed/entry} element of a Google Sheets
     * worksheet feed, extracting the {@link Tab#CELLS} link.
     * @param login the Google API Account
     * @param node the {@code feed/entry} {@link Node}
     */
    public Tab(String login, Node node) {
        try {
            login(login);
            id(Sheet.XPATH.evaluate("id", node));
            title(Sheet.XPATH.evaluate("title", node));
            feed(Sheet.XPATH.evaluate("link[@rel='"+CELLS+"']/@href", node));
        } catch (XPathExpressionException e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Downloads and parses the cells feed for this Tab, returning a
     * sorted set of {@link Cell}s.  Once the cells have been downloaded,
     * they are cached for subsequent calls.
     * @return a sorted set of {@link Cell}s
     */
    public NavigableSet<Cell> cells() {
        if (cells==null) {
            cells = new TreeSet<>();
            Document xml = null;
            try {
                String response = new UrlBuilder(feed())
                                      .auth(Tokens.get(login).googleAuth)
                                      .get();
                xml = Sheet.BUILDER.parse(new InputSource(new StringReader(response)));
                // TODO: a SAX parse will be waaaay faster than this, but XPath is fun, no?
                NodeList entries = (NodeList) Sheet.XPATH.evaluate("/feed/entry", xml, XPathConstants.NODESET);
                for (int i=0; i<entries.getLength(); i++) {
                    Cell cell = new Cell(entries.item(i));
                    cells.add(cell);
                }
                rows = cells.stream().collect(Collectors.summarizingInt(Cell::row)).getMax();
                cols = cells.stream().collect(Collectors.summarizingInt(Cell::col)).getMax();
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }
        return cells;
    }
    /**
     * Returns the maximum row index found on the tab, downloading
     * the cells if they have not been downloaded already.
     * @return the maximum row index
     */
    public int rows() {
        cells();
        return rows;
    }
    /**
     * Returns the maximum column index found on the tab, downloading
     * the cells if they have not been downloaded already.
     * @return the maximum column index
     */
    public int cols() {
        cells();
        return cols;
    }
    /**
     * Returns the cells in a row in order (by column) as a Stream.
     * @param i the row index
     * @return the cell stream
     */
    public Stream<Cell> row(int i) {
        return cells().stream().filter((k)->k.row()==i);
    }
    /**
     * Returns the cells in a column in order (by row) as a Stream.
     * @param i the column index
     * @return the cell stream
     */
    public Stream<Cell> col(int i) {
        return cells().stream().filter((k)->k.col()==i);
    }
}
