package com.cleo.labs.pojo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class Tokens {
    private static final File               TOKEN_FILE = Paths.get(System.getenv("HOME"), ".build", "google-api.yaml").toFile();
    private static final ObjectMapper       MAPPER     = new ObjectMapper();
    private static final YAMLFactory        YAML       = new YAMLFactory();
    private static       Map<String,Tokens> tokens;

    /*------------------------------------------------------------------------*\
     * Ordinary Jackson-ified fluent setter/getter beany stuff.               *
    \*------------------------------------------------------------------------*/

    private String login         = null;
    private String client_id     = null;
    private String client_secret = null;
    private String access_token  = null;
    private String token_type    = null;
    private int    expires_in    = 0;
    private String refresh_token = null;

    @JsonProperty("login")
    public String login        (                    ) { return this.login        ;                       }
    @JsonProperty("login")
    public Tokens login        (String login        ) { this.login         = login        ; return this; }
    @JsonProperty("client_id")
    public String client_id    (                    ) { return this.client_id    ;                       }
    @JsonProperty("client_id")
    public Tokens client_id    (String client_id    ) { this.client_id     = client_id    ; return this; }
    @JsonProperty("client_secret")
    public String client_secret(                    ) { return this.client_secret;                       }
    @JsonProperty("client_secret")
    public Tokens client_secret(String client_secret) { this.client_secret = client_secret; return this; }
    @JsonProperty("access_token")
    public String access_token (                    ) { return this.access_token ;                       }
    @JsonProperty("access_token")
    public Tokens access_token (String access_token ) { this.access_token  = access_token ; return this; }
    @JsonProperty("token_type")
    public String token_type   (                    ) { return this.token_type   ;                       }
    @JsonProperty("token_type")
    public Tokens token_type   (String token_type   ) { this.token_type    = token_type   ; return this; }
    @JsonProperty("expires_in")
    public int    expires_in   (                    ) { return this.expires_in   ;                       }
    @JsonProperty("expires_in")
    public Tokens expires_in   (int    expires_in   ) { this.expires_in    = expires_in   ; return this; }
    @JsonProperty("refresh_token")
    public String refresh_token(                    ) { return this.refresh_token;                       }
    @JsonProperty("refresh_token")
    public Tokens refresh_token(String refresh_token) { this.refresh_token = refresh_token; return this; }

    public String toString () {
        try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace(System.err);
        }
        return "";
    }
    /**
     * Sets all values parsed from {@code content} and saves
     * them in the file.
     * @param content JSON content to parse
     * @return {@code this}
     */
    public Tokens set(String content) {
        setOnly(content);
        save();
        return this;
    }

    /**
     * Sets all values parsed from {@code content} but does
     * not save them in the file.
     * @param content JSON content to parse
     * @return {@code this}
     */
    private Tokens setOnly(String content) {
        if (content!=null) {
            try {
                tokens.remove(login());
                JsonNode tree = MAPPER.readTree(content);
                Tokens   test = MAPPER.convertValue(tree, Tokens.class);
                if (test.login         != null) login        (test.login);
                if (test.client_id     != null) client_id    (test.client_id);
                if (test.client_secret != null) client_secret(test.client_secret);
                if (test.access_token  != null) access_token (test.access_token);
                if (test.token_type    != null) token_type   (test.token_type);
                if (test.expires_in    != 0   ) expires_in   (test.expires_in);
                if (test.refresh_token != null) refresh_token(test.refresh_token);
                tokens.put(login(), this);
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }
        return this;
    }
    
    /*------------------------------------------------------------------------*\
     * File reading/writing for the ~/.build/google_api.yaml file (static).   *
    \*------------------------------------------------------------------------*/

    /**
     * Serializes the values to JSON and writes the file.
     * @return {@code this}
     */
    private static void save() {
        try {
            JsonNode node = MAPPER.convertValue(tokens, JsonNode.class);
            String content = new ObjectMapper(YAML).writeValueAsString(node);
            File parent = new File(TOKEN_FILE.getParent());
            if (!parent.exists()) {
                parent.mkdirs();
            }
            Files.write(content, TOKEN_FILE, Charsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Loads and parses the file.
     * @return {@code this}
     */
    private static void load() {
        try {
            JsonNode node = MAPPER.readTree(YAML.createParser(TOKEN_FILE));
            tokens = MAPPER.convertValue(node, new TypeReference<Map<String,Tokens>>() {});
        } catch (IOException e) {
            e.printStackTrace(System.err);
            tokens = null;
        }
    }
    static {
        load();
    }

    /*------------------------------------------------------------------------*\
     * Google OAuth 2.0 API invocation handlers.                              *
    \*------------------------------------------------------------------------*/

    /**
     * Space separated list of scopes for which to request access.
     */
    static final String SCOPE = "https://spreadsheets.google.com/feeds";

    /**
     * This is the Redirect URI for installed applications.
     * If you are building a web application, you have to set your
     * Redirect URI at https://code.google.com/apis/console.
     */
    static final String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

    /**
     * The Google user should login and navigate to this URL.  After approving
     * access, Google with display the resulting auth_code.  The auth_code
     * is used to generate acess and refresh tokens.
     * @return a {@link UrlBuilder}
     */
    public UrlBuilder authURL() {
        return new UrlBuilder("https://accounts.google.com/o/oauth2/auth")
                   .opt("response_type", "code")
                   .opt("client_id",     client_id())
                   .opt("redirect_uri",  REDIRECT_URI)
                   .opt("scope",         SCOPE)
                   .opt("access_type",   "offline")
                   .opt("login_hint",    login());
    }

    /**
     * This URL is used to convert the auth_code to an access_token.  The
     * returned object looks like:
     * <pre>
     * {
     *    "access_token": "ya29.uAFEBGVXuWh2qIsvsV2pgEhtGdVO2C0XAGxjFSL2tCL2jeHju0EjZafAuNBzHO_rXWwS",
     *    "token_type": "Bearer",
     *    "expires_in": 3600,
     *    "refresh_token": "1/QPAVheiTnvKQ0IE88EeyAhD00ykTSIJPKwthZnm-hX4"
     * }
     * </pre>
     * @param auth_code the auth code as described under {@link #authURL()}
     * @return a {@link UrlBuilder}
     */
    public UrlBuilder accessFromAuthURL(String auth_code) {
        return new UrlBuilder("https://www.googleapis.com/oauth2/v3/token")
                   .opt("code",          auth_code)
                   .opt("client_id",     client_id())
                   .opt("client_secret", client_secret())
                   .opt("redirect_uri",  REDIRECT_URI)
                   .opt("grant_type",    "authorization_code");
    }

    /**
     * When the access token expires, this URL is used with the refresh
     * token to obtain a fresh access token.  The returned object looks like:
     * <pre>
     * {
     *    "access_token": "ya29.uQH-waCq5ZpFA9P1Obi15tAo6gAldOYBNiVIeTEsv9SheAPsgHPFFIm9Ti1itb6GT331Gg",
     *    "token_type": "Bearer",
     *    "expires_in": 3600
     * }
     * </pre>
     * @return a {@link UrlBuilder}
     */
    public UrlBuilder accessFromRefreshURL() {
        return new UrlBuilder("https://www.googleapis.com/oauth2/v3/token")
                   .opt("refresh_token", refresh_token())
                   .opt("client_id",     client_id())
                   .opt("client_secret", client_secret())
                   .opt("grant_type",    "refresh_token");
    }

    /**
     * An Auth provider based on Google tokens from this Tokens object.
     */
    @JsonIgnore
    public UrlBuilder.Auth googleAuth = new UrlBuilder.Auth () {
        @Override
        public String token() {
            return access_token();
        }
        @Override
        public void refresh() {
            String access_token = accessFromRefreshURL().post();
            set(access_token);
        }
    };

    /*------------------------------------------------------------------------*\
     * Suppressed (private) constructor and get factory.                      *
    \*------------------------------------------------------------------------*/

    /**
     * Private constructor -- use {@link #get(String)}
     */
    private Tokens() {}

    /**
     * Returns the {@link Tokens} for the requested login id,
     * or a new empty Tokens if no such entry exists.
     * @param login the Google login id, or {@code null}
     * @return the {@link Tokens} for {@code login}
     */
    public static Tokens get(String login) {
        Tokens result = tokens.get(login);
        if (result==null) {
            result = new Tokens().login(login);
        }
        return result;
    }
}