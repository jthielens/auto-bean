package com.cleo.labs.pojo;

import java.io.FileInputStream;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;

public class CuPrinter {

    private static final String TESTFILE = "/Users/jthielens/Documents/workspace/EFSS"+
                                           "/UtilitiesAndServices/LexAPI_POJO/src/main/java"+
    		                               "/com/cleo/lexicom/external/pojo/Connection.java";
    public static boolean validIdentifier(String identifier) {
    	try {
    		JavaParser.parseBodyDeclaration("int "+identifier+";");
    		return true;
    	} catch (ParseException e) {
    		return false;
    	}
    }
    public static void main(String[] args) throws Exception {
        // creates an input stream for the file to be parsed
        FileInputStream in = new FileInputStream(TESTFILE);

        CompilationUnit cu;
        try {
            // parse the file
            cu = JavaParser.parse(in);
        } finally {
            in.close();
        }

        // prints the resulting compilation unit to default system output
        System.out.println(cu.toString());
    }
}
