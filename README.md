# Generating Code from a Google Spreadsheet #

The goal of this project is to generate code for a REST API, so mostly
beans in some convention or another, from property lists managed in a
Google Spreadsheet.

The current state of the project includes reading Google Spreadsheets
through the OAuth 2.0 REST API, but nothing more.

This project depends on a command line parser library that is part of
Cleo Labs, so access to Cleo's Nexus server (currenly behind a VPN) is
required.

Note that **Java 8** is **required**.

## Executive Summary ##

```
$ java -version
(make sure it says 1.8)
$ git clone git@bitbucket.org:jthielens/auto-bean.git
$ cd auto-bean
$ mvn clean compile exec:java
[INFO] Maven does its thing...
[INFO] --- exec-maven-plugin:1.4.0:java (default-cli) @ auto-bean ---
Command processor initialized
[] login mygoogleid@example.net
From https://console.developers.google.com, create a project
and Add Credentials for an OAuth 2.0 client ID of type Other.
Enter client id:
(enter the value from Google)
Enter client secret:
(enter the value from Google)
Use the following URL to obtain an access code:
https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=...&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://spreadsheets.google.com/feeds&access_type=offline&login_hint=mygoogleid@example.net
Enter access code:
(enter the value from Google)
    mygoogleid@example.net logged in
[] sheets
    My First Spreadsheet
    My Other Spreadsheet
[] select 'My First Spreadsheet'
    sheet My First Spreadsheet selected
[] tabs
    Tab1
    Other Tab
[] select '' Tab1
    tab Tab1 on sheet My First Spreadsheet selected
[] size
    rows=94 cols=14 count=557 density=42%
[] row 1
    Cell {title="A1" row=1 col=1 input=Winken number= value=Winken}
    Cell {title="B1" row=1 col=2 input=Blinken number= value=Blinken}
    Cell {title="C1" row=1 col=3 input=Nod number= value=Nod}
[] quit
```

## Setting up Google and OAuth 2.0 ##

### First: at Google ###

First, make sure you have a Google ID (it does not have to be `@gmail.com`).  And make sure you have at least one Spreadsheet in your [Google Drive](https://drive.google.com/drive).

You will be generating a token that allows the application to access your spreadsheets on your behalf.  You must enable this by creating a Project (or using an existing one) in your [Google Developers Console](https://console.developers.google.com).  Once you have selected a Project, use the navigation panel on the left to select `Credentials` under `APIs & auth`.

By default you will be on the `Credentials` subtab (not the `OAuth consent screen` one).  From here, select `Add Credentials` and then `OAuth 2.0 client ID`.  When presented with the list of radio button options, select `Other` and click `Create`.  You will be asked to create a `Name`, but the name you choose is not important.

Google will create client ID for you and will display a popup window:

```
OAuth client

Here is your client id
[some text]

Here is your client secret
[some text]
```

Keep this window open (or copy/paste the values into some place to save for the next step).

### Second: at the command line ###

You will enter these values into the command line utility, which will use them to generate an OAuth 2.0 access token and refresh token, storing the needed values in a YAML file at `$HOME/.build/google-api.yaml`.  First, run the utility from Maven:

```
mvn exec:java
```

The program loads (downloading and compiling as needed by Maven) until you get to the `[]` prompt.  At this prompt enter the `login` command followed by your Google ID (an email address):

```
[] login mygoogleid@example.net
```

The program detects that it does not have cached tokens for this id, and prompts you for the values displayed on the Google OAuth client screen:

```
Enter client id:
(enter the value from Google)
Enter client secret:
(enter the value from Google)
```

The progam uses the values to calculate a URL from which you can authorize the `Auto Bean` program and create an access token.  First, copy the URL displayed:

```
Use the following URL to obtain an access code:
https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=...&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://spreadsheets.google.com/feeds&access_type=offline&login_hint=mygoogleid@example.net
```

### Third: back at Google ###

Paste the `https` URL into your browser, logging in to your Google ID again if required.  You will be presented with a consent screen asking if it is ok for this program to *View and manage your spreadsheets in Google Drive*.  Click `Allow	` and Google will display the access code.  Copy the access code and return to the command line.

### Fourth: back a the command line ###

The program is waiting for your input.  Paste the access code you copied from Google:

```
Enter access code:
(enter the value from Google)
    mygoogleid@example.net logged in
```

The initial setup is complete.  You can examine the resulting tokens by inspecting the `$HOME/.build/google-api.yaml` file.  In subsequent runs, the `login mygoogleid@example.net` command will complete without further interaction, loading the needed tokens from the YAML file.

Note that Google API tokens are typically valid for an hour.  The program will automatically refresh the access token whenever Google responds with a `401` (well, it will try once), updating the YAML file with the new access token.

## Browsing your Spreadsheets ##

### Command Summary ###

```
[] help
Commands                                                    
------------------------------------------------------------
cells  [sheet-title [tab-title]] list cells                 
col    i                         display column i           
exit                             exit                       
login  id                        login using Google id      
quit                             exit                       
row    i                         display row i              
select sheet-title [tab-title]   select sheet or tab        
sheets                           list sheets                
size   [sheet-title [tab-title]] stats on a tab             
tabs   [sheet-title]             list tabs of selected sheet
```

### Description ###

You navigate your collection of spreadsheets by title.  Note that technically you can have more than one spreadsheet of the same title, so don't do that or you will get one at random using this tool.

Each spreadsheet has a set of tabs, which then contain a sparse grid of cells.  Tabs are also navigated by title (again, avoid duplicates for use with this tool).

List your collection of sheets by title using `sheets`.

List the tab titles on a sheet by using `tabs` followed by the sheet title (you can use typical quoting and backslashing conventions to protect things like spaces).  If you do not supply the sheet title, the last used sheet title will be used.

List the cells on a tab by using `cells` followed by the sheet title and the tab title.  Missing (or blank, denoted by `''` or `""`) titles mean *use the last sheet or tab title I entered*.  So `cells '' 'Tab 2'` will display the cells on the second tab in the same sheet you are already working in.

You can select a sheet and/or tab without listing the contents using the `select` command.  Use `select '' ''` to display the currently selected sheet and tab.

In addition to listing all the cells on a tab, you can display the size of the tab using `size` as follows:

```
[] size
    rows=94 cols=14 count=557 density=42%
```

Rows and columns are numbered from 1.  You can display slices through the cell grid using `row i` and `col i`.

Cells are displayed as follows:

```
Cell {title="A1" row=1 col=1 input=Winken number= value=Winken}
```

The `input` attribute represents the text or formula in the cell as entered.  The `value` attribute contains the calculated result, which is identical to the `input` in the case of a simple text cell.  The `number` attribute contains the numeric value of the cell if it is determined to be numeric.  Cell `title`s are based on the `A1` notation, while the `row` and `col` attributes use numeric indices.

Exit the command line processor with `exit` or `quit`.
